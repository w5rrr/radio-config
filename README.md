Radio Configurations
====================

Here you will find various contributed radio configuration files. They will be
organized in folders by protocol, and each file will be named with the format
`CONTRIBUTOR_RADIO_DASH-SEPARATED-DESCRIPTION`.

**IMPORTANT**: Please ensure that you change any embedded callsign, name, or
other unique ID in the configuration files before use.

